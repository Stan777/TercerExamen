package com.example.BlogRestaurante.entities;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "pedido")
public class Pedido {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "nombredepedido")
    private String nombre_Pedido;
    @Column(name = "preciopedido")
    private Integer precio_Pedido;

    @Column(name = "direccion")
    private String direccion;



    @Column(name = "calle")
    private String calle_Pedido;

    @Column(name = "Numero_de_casa")
    private Integer numero_Casa_Pedido;

    @ManyToOne
    @JoinColumn(name = "Zona")
    private Restaurant zona = null;

    @Range(min = 1,max = 5,message = "Debe ser mayor a 1 y menor a 5 digitos")
    @Column(name = "Zip_Code_Pedido")
    private Integer Zip_Code_Pedido;











    @ManyToOne
    @JoinColumn(name = "category_restaurant")
    private Restaurant category_restaurant = null;

    @ManyToOne
    @JoinColumn(name = "category_choice")
    private Choice category_choice;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User id_user;

    public Pedido(User user){
        this.id_user=user;
    }

    public Pedido(){};

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre_Pedido() {
        return nombre_Pedido;
    }

    public void setNombre_Pedido(String nombre_Pedido) {
        this.nombre_Pedido = nombre_Pedido;
    }

    public Integer getPrecio_Pedido() {
        return precio_Pedido;
    }

    public void setPrecio_Pedido(Integer precio_Pedido) {
        this.precio_Pedido = precio_Pedido;
    }

    public Restaurant getCategory_restaurant() {
        return category_restaurant;
    }

    public void setCategory_restaurant(Restaurant category_restaurant) {
        this.category_restaurant = category_restaurant;
    }


    public Choice getCategory_choice() {
        return category_choice;
    }

    public void setCategory_choice(Choice category_choice) {
        this.category_choice = category_choice;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    public User getId_user() {
        return id_user;
    }

    public void setId_user(User id_user) {
        this.id_user = id_user;
    }





    public String getCalle_Pedido() {
        return calle_Pedido;
    }

    public void setCalle_Pedido(String calle_Pedido) {
        this.calle_Pedido = calle_Pedido;
    }

    public Integer getNumero_Casa_Pedido() {
        return numero_Casa_Pedido;
    }

    public void setNumero_Casa_Pedido(Integer numero_Casa_Pedido) {
        this.numero_Casa_Pedido = numero_Casa_Pedido;
    }

    public Restaurant getZona() {
        return zona;
    }

    public void setZona(Restaurant zona) {
        this.zona = zona;
    }

    public Integer getZip_Code_Pedido() {
        return Zip_Code_Pedido;
    }

    public void setZip_Code_Pedido(Integer zip_Code_Pedido) {
        Zip_Code_Pedido = zip_Code_Pedido;
    }
}
