package com.example.BlogRestaurante.controllers;

import com.example.BlogRestaurante.entities.Choice;
import com.example.BlogRestaurante.entities.Pedido;

import com.example.BlogRestaurante.entities.User;
import com.example.BlogRestaurante.services.ChoiceService;
import com.example.BlogRestaurante.services.PedidoService;


import com.example.BlogRestaurante.services.RestaurantService;
import com.example.BlogRestaurante.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.ArrayList;


@Controller
public class PedidoController {

    @Autowired
    private PedidoService pedidoService;


    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ChoiceService choiceService;


    @Autowired
    private UserService userService;

    @RequestMapping(value="/pedidos",method = RequestMethod.GET)
    public String listas (Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());


        if(user.isIdentificadorOpcion() == false){
            model.addAttribute("pedido", new Pedido());
            model.addAttribute("restaurants", restaurantService.listAllRestaurants());
            model.addAttribute("choices", choiceService.listAllOptions());
            //model.addAttribute("users",userService.listAllUsers());
            model.addAttribute("users",user); //cargando ID

            return "newPedido";
        }

        model.addAttribute("pedidos", pedidoService.listAllOptions());
        //model.addAttribute("users",userService.listAllUsers());
        model.addAttribute("users",userService.getUserById(user.getId()));
        model.addAttribute("users",user);  //Cargando ID

        return "redirect:/user/pedidos";
    }


    @RequestMapping(value = "/pedido/new", method = RequestMethod.GET)
    public String newPedido(Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        //se crea un objeto new pedido
        model.addAttribute("pedido", new Pedido());
        model.addAttribute("restaurants", restaurantService.listAllRestaurants());
        model.addAttribute("choices", choiceService.listAllOptions());
        model.addAttribute("users",user);

        return "newPedido";
    }

    @RequestMapping(value = "/nextPedido", method = RequestMethod.POST)
    public String nextPedido(@Valid Pedido pedido, BindingResult bindingResult, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        Iterable<Choice> allChices = choiceService.listAllOptions();
        ArrayList<Choice> listChoices = new ArrayList<>();

        System.out.println("category restaurant: " + pedido.getCategory_restaurant().getId());

        for (Choice c : allChices) {
            if (c.getCategory_restaurant().getId() == pedido.getCategory_restaurant().getId()) {
                listChoices.add(c);
            }
        }

        model.addAttribute("pedido", pedido);
        model.addAttribute("choices", listChoices);
        model.addAttribute("users", user);

        return "nextPedido";
    }


    @RequestMapping(value = "/pedido", method = RequestMethod.POST)
    public String save(@Valid Pedido pedido, BindingResult bindingResult, Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        Iterable<Pedido> pedidos = pedidoService.listAllOptions();

        user.setIdentificadorOpcion(true);

        pedido.setId_user(user);
        pedidoService.saveRestaurant(pedido);
        userService.saveUserEdited(user);

        //model.addAttribute("user",user);

        return "redirect:/pedidos";
    }

    @RequestMapping(value = "/user/pedidos", method = RequestMethod.GET)
    public String listOfPedidos( Model model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        model.addAttribute("pedidos", pedidoService.listAllOptions());
        model.addAttribute("user",user);
        //model.addAttribute("pedido", new Pedido(user));
        //model.addAttribute("users", userService.listAllUsers());



        //return "listsPedidos";
        return "optionsPedidos";


    }

    @RequestMapping(value = "/pedido/edit/{id}", method = RequestMethod.GET)
    public String editPedido(@PathVariable Integer id, Model model) {
        model.addAttribute("pedido", pedidoService.getRestaurantById(id));

        return "editDireccion";
    }


    @RequestMapping(value = "/pedido/{id}", method = RequestMethod.GET)
    public String showPedido(@PathVariable Integer id, Model model) {

        model.addAttribute("pedido", pedidoService.getRestaurantById(id));
        model.addAttribute("choice", choiceService.getRestaurantById(id));
        return "mostrarPedido";
    }



















}
